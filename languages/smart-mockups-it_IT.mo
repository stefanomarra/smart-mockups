��    *      l  ;   �      �     �     �     �     �     �     �     �     	          &  K   7     �     �     �     �  
   �     �     �     �  '   �  #     >   2  	   q     {     �     �     �  
   �  
   �  	   �     �     �     �     �          .     <  !   C     e     q     �  �  �     W	     i	     �	     �	     �	  
   �	     �	     �	     �	     
  J   
     f
     o
     v
     �
     �
     �
     �
     �
  +   �
  ,     >   I     �  	   �     �     �     �     �     �               0     @  '   V     ~     �     �  .   �     �     �                    "   &                                  *       !                 (                    '             	                                               
   %             $         #             )    Add New Add New Mockup All Mockups Allow Discussion Allow Feedbacks Approve Approve this Mockup Approve this mockup Approved Background Color By entering the digital signature below, you approve the underneath mockup. Cancel Close Custom Slug Digital Signature Dots Color Edit Mockup Enable Approval Enable Help Text Enable the discussion panel on the side Enable the point and click feedback Enable the user to approve the mockup with a digital signature Help Text Mockup Mockup Approved Mockup Discussion Mockup Image Need Help? New Mockup Password: Post this Comment Search Mockups Show Credits Show/Hide Discussion Panel Show/Hide Feedbacks Smart Mockups Submit Upload or select the mockup image View Mockup Write a comment... by Project-Id-Version: Smart Mockups
POT-Creation-Date: 2016-02-16 11:39+0100
PO-Revision-Date: 2016-02-16 11:43+0100
Last-Translator: Stefano Marra <stefano.marra1987@gmail.com>
Language-Team: Stefano Marra <stefano.marra1987@gmail.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.1
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 ﻿Aggiungi Nuovo ﻿Aggiungi Nuovo Mockup ﻿Tutti i Mockup Abilita Discussione Abilita Feedbacks ﻿Approva ﻿Approva questo Mockup ﻿Approva questo mockup ﻿Approvato ﻿Colore dello Sfondo ﻿Inserendo la firma digitale in basso, si approva il mockup sottostante. Cancella Chiudi ﻿URL Slug Personalizzato ﻿Firma Digitale Colore dei Punti Modifica Mockup Abilita Approvazione Abilita Testo di Aiuto Abilita il pannello di discussione laterale Abilita la funziona point and click feedback Abilita l'utente di approvare il mockup con una firma digitale Testo di Aiuto ﻿Mockup Mockup ﻿Approvato ﻿Discussione Mockup Immagine di Mockup ﻿Hai bisogno di aiuto? ﻿Nuovo Mockup ﻿Password: ﻿Pubblica questo commento ﻿Cerca Mockup ﻿Visualizza Crediti ﻿Mostra/Nascondi Pannello Discussione Mostra/Nascondi Feedbacks Smart Mockups ﻿Invia ﻿Caricare o selezionare l'immagine di mockup ﻿Visualizza Mockup ﻿Scrivi un commento... ﻿da 